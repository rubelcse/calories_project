//Storage controller



//Item controller
const ItemCtrl = (() => {
  //item constructor
  const Item = function (id, name, calories) {
    this.id = id;
    this.name = name;
    this.calories = calories;
  }

  const data = {
    items: [{
        id: 1,
        name: 'banana',
        calories: 1200
      }
      // {id : 2, name : 'masrume', calories : 600},
      // {id : 3, name : 'egg', calories : 500}
    ],
    currentItems: null,
    totalCalories: 0
  }

  //public mehtod
  return {
    getItems: function () {
      return data.items;
    },

    addItem: function (name, calories) {
      let ID;
      if (data.items.length > 0) {
        ID = data.items[data.items.length - 1].id + 1;
      } else {
        ID = 0;
      }
      const calorie = parseInt(calories);
      newItem = new Item(ID, name, calorie);
      data.items.push(newItem);
      return newItem;
    },

    getItemById: (id) => {
      const item = data.items.find((item) => {
        return item.id == id;
      });
      return item;
    },

    setCurrentItem: (item) => {
      data.currentItems = item;
    },

    getTotalCalories: () => {
      let total = 0;
      data.items.forEach((item) => {
        total += item.calories;
      });
      //set the data structecture totalcalories
      data.totalCalories = total;
      return total;
    },

    getCurrentItem: () => {
      return data.currentItems;
    },

    logData: function () {
      return data;
    }
  }


})();

//UI controller
const UICtrl = (() => {

  const UISelectors = {
    itemList: '#item-list',
    addBtn: '.add-btn',
    updateBtn: '.update-btn',
    deleteBtn: '.delete-btn',
    backBtn: '.back-btn',
    itemName: '#item-name',
    itemCalories: '#item-calories',
    itemTotalCalories: '.total-calories'
  }

  //public mehtod
  return {
    //added the collection of items.
    populateView: function (items) {
      let html = '';
      items.forEach((item) => {
        html += `
        <li class="collection-item" id="item-${item.id}">
          <strong>${item.name}: </strong> <em>${item.calories} Calories</em>
          <a href="#" class="secondary-content">
            <i class="edit-item fa fa-pencil"></i>
          </a>
        </li> 
        `;
      });
      //added the view into item list
      document.querySelector(UISelectors.itemList).innerHTML = html;
    },
    //add a single item.
    addNewItems: (item) => {
      //visible the list
      document.querySelector(UISelectors.itemList).style.display = 'block';
      //create a new item
      const li = document.createElement('li');
      li.className = 'collection-item';
      li.id = `item-${item.id}`;
      li.innerHTML = `
          <strong>${item.name}: </strong> <em>${item.calories} Calories</em>
          <a href="#" class="secondary-content">
            <i class="edit-item fa fa-pencil"></i>
          </a>
      `;

      //intsert new item into the view
      document.querySelector(UISelectors.itemList).insertAdjacentElement('beforeend', li);
    },

    addTotalCalories: (total) => {
      document.querySelector(UISelectors.itemTotalCalories).innerText = total;
    },

    clearUpdateState: () => {
      document.querySelector(UISelectors.addBtn).style.display = 'inline';
      document.querySelector(UISelectors.updateBtn).style.display = 'none';
      document.querySelector(UISelectors.deleteBtn).style.display = 'none';
      document.querySelector(UISelectors.backBtn).style.display = 'none';
    },

    visibleUpdateState: () => {
      document.querySelector(UISelectors.addBtn).style.display = 'none';
      document.querySelector(UISelectors.updateBtn).style.display = 'inline';
      document.querySelector(UISelectors.deleteBtn).style.display = 'inline';
      document.querySelector(UISelectors.backBtn).style.display = 'inline';
    },

    clearInput: () => {
      document.querySelector(UISelectors.itemName).value = '';
      document.querySelector(UISelectors.itemCalories).value = '';
    },

    addEditItem: () => {
      document.querySelector(UISelectors.itemName).value = ItemCtrl.getCurrentItem().name;
      document.querySelector(UISelectors.itemCalories).value = ItemCtrl.getCurrentItem().calories;
      //visible edit state
      UICtrl.visibleUpdateState();
    },

    hideList: () => {
      document.querySelector(UISelectors.itemList).style.display = 'none';
    },

    getSelectors: () => {
      return UISelectors;
    },

    getItem: () => {
      return {
        name: document.querySelector(UISelectors.itemName).value,
        calories: document.querySelector(UISelectors.itemCalories).value
      }
    }
  }
})();


//App controller

const App = ((ItemCtrl, UICtrl) => {
  //Load addeventlistenrs
  const loadEventListener = () => {
    const UISelectors = UICtrl.getSelectors();

    //add item
    document.querySelector(UISelectors.addBtn).addEventListener('click', itemAddSubmit);

    //edit item 
    document.querySelector(UISelectors.itemList).addEventListener('click', itemUpdateSubmit);
  }

  //add item submit
  const itemAddSubmit = (e) => {
    //get the user input
    const item = UICtrl.getItem();
    if (item.name !== '' && item.calories !== '') {
      const newItem = ItemCtrl.addItem(item.name, item.calories);
      //add the new item into the ui
      UICtrl.addNewItems(newItem);

      //added the total calories into the ui
      const totalCalories = ItemCtrl.getTotalCalories();
      UICtrl.addTotalCalories(totalCalories);


      //clear the input 
      UICtrl.clearInput();

    }
    e.preventDefault();
  }

  const itemUpdateSubmit = (e) => {
    if (e.target.classList.contains('edit-item')) {
      const itemId = e.target.parentNode.parentNode.id;

      //item id conver to array and get the actual id
      const itemIdArray = itemId.split('-');
      const id = itemIdArray[1];

      //get the actual item
      const item = ItemCtrl.getItemById(id);

      //set the curernt item in data source
      ItemCtrl.setCurrentItem(item);

      //add the update view
      UICtrl.addEditItem();

    }
    e.preventDefault();
  }

  //public method
  return {
    init: function () {
      //clear the update state and view the initial state
      UICtrl.clearUpdateState();

      //item list form data structrue
      const items = ItemCtrl.getItems();

      if (items.length === 0) {
        UICtrl.hideList();
      } else {
        //added the total calories into the ui
        const totalCalories = ItemCtrl.getTotalCalories();
        UICtrl.addTotalCalories(totalCalories);

        //populate the view
        UICtrl.populateView(items);
      }
      //load initial event listener
      loadEventListener();
    }
  }
})(ItemCtrl, UICtrl);

//initially call
App.init();